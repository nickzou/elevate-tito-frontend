
window.addEventListener('DOMContentLoaded',()=>{
    let siteLoader = document.getElementById('site-loader');
    let form = document.getElementById('filter-form');
    let accordionToggle = document.getElementById('accordion-toggle');

    siteLoader.classList.add('hide');

    accordionToggle.addEventListener('click', function() {
        if (!!this.classList.contains('active')) {
            this.innerHTML = 'hide filters';
        } else {
            this.innerHTML = 'show filters';
        }
        this.classList.toggle('active');
        form.classList.toggle('active-mobile');
    });
});